"""Exporting all errors from the domain layer."""

from .generic import (
    group_errors,
    InternalError,
    ResourceNotFoundError,
    ResourceAlreadyExists,
    BadRequestError,
    ForbiddenRequestError,
    BadDatabaseConnection,
)

base_errors = {
    **group_errors(500, [
        InternalError()
    ])
}
