# pylint: disable=too-few-public-methods
"""Report schemas"""
from pydantic import Field
from .generic import GenericSchema


class ReportSchema(GenericSchema):
    """Report output schema."""
    opponent_id: str = Field(
        title='Opponent id (email)',
        examples=["opponent@email.com"],
    )
    matches: list[dict] = Field(
        title='Matches with the opponent',
        examples=[{
            "matchId": "df5b534f-b3bd-4958-9927-29838519fdf1",
            "myRank": "Royal Flush",
            "result": "WIN",
            "myMatchBalance": -100,
        }],
    )
    total_balance_with_opponent: int = Field(
        title='Total balance with the opponent',
        examples=[500],
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
