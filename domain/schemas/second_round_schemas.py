# pylint: disable=too-few-public-methods
"""Second round schemas"""
from uuid import UUID
from pydantic import Field
from .generic import GenericSchema


class SecondRoundInput(GenericSchema):
    """Second round input schema by the dealer."""
    match_id: UUID = Field(
        title='Match UUID',
        examples=["df5b534f-b3bd-4958-9927-29838519fdf1"],
        description="Match UUID informed by the dealer."
    )
    new_cards: list[str] = Field(
        title='Cards',
        examples=[
            ["3c", "5c", "5s"]  # SWAP - One Pair example
        ],
        description="List of the new cards you chose to swap. Again following the "
                    "pattern 'value' + 'suit'. Value can be from 2 to 9, T, J, Q, K, A. "
                    "Suit can be h (copas), d (ouros), s (espadas), c (paus)."
    )
    match_bet: int = Field(
        title='Total value you are betting found from the first round',
        examples=[150],  # One Pair example
        description="From this value you can check how much your opponent "
                    "bet and decide if you want to fold."
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
        schema_extra = {
            "examples": {
                "Royal Flush": {
                    "summary": "Royal Flush example",
                    "description": "Royal Flush example",
                    "value": {
                        "matchId": "df5b534f-b3bd-4958-9927-29838519fdf1",
                        "newCards": [],
                        "matchBet": 250
                    }
                },
                "Straight Flush": {
                    "summary": "Straight Flush example",
                    "description": "Straight Flush example",
                    "value": {
                        "matchId": "4628e097-30f9-45de-a860-2005e088590c",
                        "newCards": [],
                        "matchBet": 270
                    }
                },
                "One Pair": {
                    "summary": "One Pair example",
                    "description": "One Pair example",
                    "value": {
                        "matchId": "f1c50a50-83a0-4b0c-aef8-ea5ac0d0b79c",
                        "newCards": ["3c", "5h", "5s"],
                        "matchBet": 100
                    }
                },
                "High Card": {
                    "summary": "High Card example",
                    "description": "High Card example",
                    "value": {
                        "matchId": "8987f0c9-b233-4bf0-950f-93180d62db8d",
                        "newCards": ["3s", "3h", "As", "Ah", "Kc"],
                        "matchBet": 200
                    }
                },
                "Straight": {
                    "summary": "Straight example",
                    "description": "Straight example",
                    "value": {
                        "matchId": "dfb31df8-034f-4000-b8b5-c0b886d2c88d",
                        "newCards": ["2h", "Jh", "Qh"],
                        "matchBet": 200
                    }
                },
                "Two Pair": {
                    "summary": "Two Pair example",
                    "description": "Two Pair example",
                    "value": {
                        "matchId": "9955a077-fed3-4737-9632-7f02d9da239b",
                        "newCards": ["2h", "Jh", "Qh"],
                        "matchBet": 200
                    }
                }
            }
        }


class SecondRoundOutput(GenericSchema):
    """My second round output schema. I must return to the dealer
    the bet I'm making for this second round."""
    bet: int = Field(
        title='Bet value for the second round',
        examples=[220],
        description="Bet value for this round (range from 0 to 200) "
                    "to be added to the total bet of the entire match."
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
