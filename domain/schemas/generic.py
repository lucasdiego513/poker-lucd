# pylint: disable=too-few-public-methods
"""Generic classes and resources for schema building."""
from pydantic import BaseModel
from pydantic.alias_generators import to_camel


class GenericSchema(BaseModel):
    """Generic Class for schemas."""

    class Config:
        """Pydantic Config class."""
        alias_generator = to_camel
        populate_by_name = True
        arbitrary_types_allowed = True


class TableSchema(GenericSchema):
    """Class for schemas based on database table."""

    class Config:
        """Pydantic Config class."""
        from_attributes = True
