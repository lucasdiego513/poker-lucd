# pylint: disable=too-few-public-methods
"""First round schemas"""
from uuid import UUID
from pydantic import Field
from .generic import GenericSchema


class FirstRoundInput(GenericSchema):
    """First round input schema by the dealer."""
    match_id: UUID = Field(
        title='Match UUID',
        description="Match UUID informed by the dealer.",
        examples=["df5b534f-b3bd-4958-9927-29838519fdf1"]
    )
    cards: list[str] = Field(
        title='Cards',
        description="List of 5 cards following the pattern 'value' + 'suit'. "
                    "Value can be from 2 to 9, T, J, Q, K, A. "
                    "Suit can be h (copas), d (ouros), s (espadas), c (paus).",
        examples=[
            ["10h", "Jh", "Qh", "Kh", "Ah"]  # Royal Flush example
        ]
    )
    opponent_id: str = Field(
        title='Opponent id (email)',
        description="Opponent id informed by the dealer.",
        examples=["ele@emai.com"]
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
        schema_extra = {
            "examples": {
                "Royal Flush": {
                    "summary": "Royal Flush example",
                    "description": "Royal Flush example",
                    "value": {"matchId": "df5b534f-b3bd-4958-9927-29838519fdf1",
                              "cards": ["Th", "Jh", "Qh", "Kh", "Ah"],
                              "opponentId": "ele@email.com"
                              }
                },
                "Straight Flush": {
                    "summary": "Straight Flush example",
                    "description": "Straight Flush example",
                    "value": {
                        "matchId": "4628e097-30f9-45de-a860-2005e088590c",
                        "cards": ["2h", "3h", "4h", "5h", "6h"],
                        "opponentId": "ele@email.com"
                    }
                },
                "One Pair": {
                    "summary": "One Pair example",
                    "description": "One Pair example",
                    "value": {
                        "matchId": "f1c50a50-83a0-4b0c-aef8-ea5ac0d0b79c",
                        "cards": ["Ah", "3s", "3h", "2h", "6h"],
                        "opponentId": "ele@email.com"
                    }
                },
                "High Card": {
                    "summary": "High Card example",
                    "description": "High Card example",
                    "value": {
                        "matchId": "8987f0c9-b233-4bf0-950f-93180d62db8d",
                        "cards": ["Ah", "3s", "5h", "2h", "6h"],
                        "opponentId": "ela@email.com"
                    }
                },
                "Straight": {
                    "summary": "Straight example",
                    "description": "Straight example",
                    "value": {
                        "matchId": "dfb31df8-034f-4000-b8b5-c0b886d2c88d",
                        "cards": ["Ah", "3s", "5h", "2h", "4h"],
                        "opponentId": "ela@email.com"
                    }
                },
                "Two Pair": {
                    "summary": "Two Pair example",
                    "description": "Two Pair example",
                    "value": {
                        "matchId": "9955a077-fed3-4737-9632-7f02d9da239b",
                        "cards": ["Ah", "3s", "4s", "3h", "4h"],
                        "opponentId": "ela@email.com"
                    }
                }
            }
        }


class FirstRoundOutput(GenericSchema):
    """My first round output schema. I must return to the dealer
    my initial bet and the cards I want to change."""
    bet: int = Field(
        title='Initial bet',
        examples=[40],  # One Pair example
        description="Initial bet value (range from 0 to 200)"
    )
    cards_to_swap: list[str] = Field(
        title='Cards to change',
        examples=[
            ["Ah", "5h", "6h"]  # One Pair example
        ],
        description="List of cards to change."
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
