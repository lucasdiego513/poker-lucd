# pylint: disable=too-few-public-methods
"""Result schemas"""
from uuid import UUID
from pydantic import Field
from .generic import GenericSchema


class ResultInput(GenericSchema):
    """Result input schema by the dealer."""
    match_id: UUID = Field(
        title='Match UUID',
        examples=["df5b534f-b3bd-4958-9927-29838519fdf1"],
        description="Match UUID informed by the dealer."
    )
    result: str = Field(
        title='Result of the match',
        examples=["WIN", "LOSS", "DRAW"],
        description="Result of the match informed by the dealer."
    )
    your_match_balance: int = Field(
        title='Your balance after the match',
        examples=[-100],
        description="Your balance after the match. You can use this value to "
                    "add to your entire balance."
    )

    class Config:
        """Pydantic config"""
        from_attributes = True
        schema_extra = {
            "examples": {
                "Royal Flush": {
                    "summary": "Royal Flush",
                    "description": "Royal Flush result example.",
                    "value": {
                        "matchId": "df5b534f-b3bd-4958-9927-29838519fdf1",
                        "result": "WIN",
                        "yourMatchBalance": -100,
                    }
                },
                "Straight Flush": {
                    "summary": "Straight Flush",
                    "description": "Straight Flush result example.",
                    "value": {
                        "matchId": "4628e097-30f9-45de-a860-2005e088590c",
                        "result": "DRAW",
                        "yourMatchBalance": 0,
                    }
                },
                "One Pair": {
                    "summary": "One Pair",
                    "description": "One Pair result example.",
                    "value": {
                        "matchId": "f1c50a50-83a0-4b0c-aef8-ea5ac0d0b79c",
                        "result": "LOSS",
                        "yourMatchBalance": 100,
                    }
                },
                "High Card": {
                    "summary": "High Card",
                    "description": "High Card result example.",
                    "value": {
                        "matchId": "8987f0c9-b233-4bf0-950f-93180d62db8d",
                        "result": "WIN",
                        "yourMatchBalance": 700,
                    }
                },
                "Straight": {
                    "summary": "Straight",
                    "description": "Straight result example.",
                    "value": {
                        "matchId": "dfb31df8-034f-4000-b8b5-c0b886d2c88d",
                        "result": "WIN",
                        "yourMatchBalance": 200,
                    }
                },
                "Two Pair": {
                    "summary": "Two Pair",
                    "description": "Two Pair result example.",
                    "value": {
                        "matchId": "9955a077-fed3-4737-9632-7f02d9da239b",
                        "result": "LOSS",
                        "yourMatchBalance": 100,
                    }
                }
            }
        }
