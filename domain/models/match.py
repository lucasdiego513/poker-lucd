# pylint: disable=too-few-public-methods
"""Model for matches data"""
from sqlalchemy import Column, String, UUID, ARRAY, Integer
from domain.models.generic import GenericBase


class Match(GenericBase):
    """Match model"""
    __tablename__ = "match"

    match_id = Column("matc_cd_match", UUID(as_uuid=True), primary_key=True)
    opponent_id = Column("matc_cd_opponent", String)
    cards = Column("matc_cd_cards", ARRAY(String))
    cards_to_swap = Column("matc_cd_cards_to_swap", ARRAY(String))
    balance = Column("matc_nr_balance", Integer)
    rank = Column("matc_cd_rank", String)
    result = Column("matc_nm_result", String)
