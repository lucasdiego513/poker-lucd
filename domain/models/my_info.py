# pylint: disable=too-few-public-methods
"""Model for my info"""
from sqlalchemy import Column, String, Integer
from domain.models.generic import GenericBase


class MyInfo(GenericBase):
    """Match model"""
    __tablename__ = "my_info"

    my_id = Column("myin_cd_my_info", String, primary_key=True)
    balance = Column("myin_nr_balance", Integer, default=0)
