"""Consuming dealer endpoints"""
import json
from httpx import Response
from requests import request as raw_request
from config import dealer_url


def join_match(join_info: json) -> Response:
    """Join a poker match."""
    url = f'{dealer_url}/join-game'

    response = raw_request(
        url=url,
        method='POST',
        headers={
            'Authorization': None,
            'Content-Type': 'application/json'
        },
        json=join_info,
        timeout=60000  # 60 seconds
    )

    return response
