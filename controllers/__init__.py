"""Controllers list"""
from . import match_controller

routes = [
    match_controller.router
]

tags = [
    *match_controller.tags
]
