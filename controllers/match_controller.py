"""Users controller module."""
from fastapi import APIRouter, Depends, Body
from sqlalchemy.orm import Session
from domain.schemas import FirstRoundInput, FirstRoundOutput, SecondRoundInput, \
    SecondRoundOutput, ResultInput, ReportSchema
from services import match_service
from config import get_session

router = APIRouter()

POKER_MATCH = {
    "name": 'Poker match - 5 card draw',
    "description": 'Poker match operations.'
}

tags = [POKER_MATCH]


@router.post(
    "/join-match",
    summary="Join a poker match",
    description="Join a poker match: I'm informing to the dealer's consumer"
                " my id (lccv email), name (Lucas Lino) and my public api url.",
    tags=[POKER_MATCH['name']],
)
def join_match() -> dict:
    """Join a poker match."""
    return match_service.join_match()


@router.post(
    "/first-round",
    summary="First round of the poker match",
    description="In the first round, the dealer will inform the match id, "
                "the 5 cards I received, and opponent's id.",
    tags=[POKER_MATCH['name']],
    response_model=FirstRoundOutput
)
def match_first_round(
        body: FirstRoundInput = Body(
            description="First round input by the dealer informing the match id, "
                        "the 5 cards I received, and opponent's id.",
            openapi_examples=FirstRoundInput.Config.schema_extra['examples']),
        session: Session = Depends(get_session)
) -> FirstRoundOutput:
    """Handle the first round of the poker match."""
    return match_service.handle_first_round(session, body)


@router.post(
    "/second-round",
    summary="Second round of the poker match",
    description="In the second round, the dealer will inform the match id, "
                "the new cards you chose to swap, and the total value you're "
                "betting in the match. From this value you can check how much your "
                "opponent bet and decide if you want to fold.",
    tags=[POKER_MATCH['name']],
    response_model=SecondRoundOutput
)
def match_second_round(
        session: Session = Depends(get_session),
        body: SecondRoundInput = Body(
            description="Second round input by the dealer informing the match id, "
                        "the new cards you chose to swap, and the total value you're "
                        "betting in the match.",
            openapi_examples=SecondRoundInput.Config.schema_extra['examples'])) \
        -> SecondRoundOutput:
    """Handle the second round of the poker match."""
    return match_service.handle_second_round(session, body)


@router.post(
    "/result",
    summary="Result of the poker match",
    description="In the result, the dealer will inform the match id, the result of the"
                " match and my balance after the match.",
    tags=[POKER_MATCH['name']]
)
def match_result(
        session: Session = Depends(get_session),
        body: ResultInput = Body(
            description="Result input schema by the dealer.",
            openapi_examples=ResultInput.Config.schema_extra['examples'])) \
        -> None:
    """Handle the result of the poker match."""
    return match_service.handle_result(session, body)


@router.get(
    "/report",
    summary="Report of all poker matches with each opponent, "
            "result and my balance after each match",
    description="Get a report of all poker matches with each opponent, "
                "result and my balance after each match.",
    tags=[POKER_MATCH['name']]
)
def get_report(session: Session = Depends(get_session)) -> list[ReportSchema]:
    """Get a report of all poker matches with each opponent, result and
    my balance after each match."""
    return match_service.get_report(session)
