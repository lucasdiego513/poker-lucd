"""Application initializing function and printing"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi
import uvicorn
from config.database import db
from config.server import base_url, port
from config import application, dealer_url
from config.middlewares import middlewares, cors
from domain.errors import base_errors
from controllers import routes, tags
from utils.valuable import valuable

DESCRIPTION = """
This is the LUCD POKER service dedicated to win the tournament 🚀

Developed by [LUCD](https://www.linkedin.com/in/lucas-lino/).
"""

app = FastAPI(
    title=application["name"],
    description=DESCRIPTION,
    version="0.0.1",
    contact={
        "name": "LUCD support",
        "email": "lucaslino@lccv.ufal.br",
    },
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
    responses=base_errors,
)

for middleware in middlewares:
    app.middleware(middleware.type)(middleware(app))
app.add_middleware(CORSMiddleware, **cors)

for router in routes:
    app.include_router(router)

openapi_schema = get_openapi(
    title=app.title,
    version=app.version,
    description=app.description,
    servers=[{"url": base_url}],
    routes=app.routes,
    tags=tags)

api = FastAPI(openapi_url=None)
api.openapi = openapi_schema
api.mount(base_url, app)

print(f'''
Service {application["id"]} is available on port {port}:

  Environment variables:

    -> Database:
        USER_DB: {db["user"]}
        HOST_DB: {db["host"]}
        PORT_DB: {db["port"]}
        SCHEMA_DB: {db["schema"]}
        NAME_DB: {db["name"]}
        IS_PASSWORD_DEFINED: {valuable(db["password"])}

    -> Server:
        SUB_DIR: {base_url}
        SERVER_PORT: {str(port)}

    -> Custom:
        APP_NAME: {application["name"]}
        DEALER_BASE_PATH: {dealer_url}

  Docs (Swagger) available on: http://localhost:{port}{base_url or ""}{app.docs_url}.
''')

if __name__ == "__main__":
    uvicorn.run('main:api',
                host='0.0.0.0',
                port=port,
                reload=True)
