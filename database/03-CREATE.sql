-- Create match table to store the poker matches data: match id (uuid), cards (list[str]), match balance (int), opponent id (str) and the result (str)
create table app_poker.match
(
    matc_cd_match uuid not null,
    matc_cd_opponent varchar not null,
    matc_cd_cards varchar[] not null check (array_length(matc_cd_cards, 1) <= 5),
    matc_cd_cards_to_swap varchar[] check (array_length(matc_cd_cards_to_swap, 1) <= 5),
    matc_nr_balance integer,
    matc_cd_rank varchar,
    matc_nm_result varchar,
    constraint pk_match primary key (matc_cd_match)
);

comment on table app_poker.match is 'Match table';
comment on column app_poker.match.matc_cd_match is 'UUID of the match';
comment on column app_poker.match.matc_cd_opponent is 'Opponent of the match';
comment on column app_poker.match.matc_cd_cards is 'Final cards of the match';
comment on column app_poker.match.matc_cd_cards_to_swap is 'Cards to swap';
comment on column app_poker.match.matc_nr_balance is 'Match balance after the match';
comment on column app_poker.match.matc_cd_rank is 'Rank of the match (Royal Flush, Straight Flush, Four of a Kind, Full House, Flush, Straight, Three of a Kind, Two Pair, One Pair, High Card)';
comment on column app_poker.match.matc_nm_result is 'Result of the match';


-- Create table to store my information: my id (str) and my balance (int)
create table app_poker.my_info
(
    myin_cd_my_info varchar not null,
    myin_nr_balance integer not null default 0,
    constraint pk_my_info primary key (myin_cd_my_info)
);

comment on table app_poker.my_info is 'My info table';
comment on column app_poker.my_info.myin_cd_my_info is 'My id';
comment on column app_poker.my_info.myin_nr_balance is 'My balance';
