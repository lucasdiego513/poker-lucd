# pylint: disable=invalid-name
"""Env var importing package"""
import os
import logging
from dotenv import load_dotenv

load_dotenv()

application = {
    "id": os.getenv('APP_ID') or 'sbwb-fast-meeting-backend',
    "name": os.getenv('APP_NAME') or 'SUBWEB FAST MEETING API',
}

my_info = {
    "id": os.getenv('MY_ID'),
    "name": os.getenv('MY_NAME'),
    "public_api_url": os.getenv('MY_PUBLIC_API_URL')
}

dealer_url = os.getenv('DEALER_BASE_PATH') or ''

debug_mode = False  # type: ignore
if os.getenv('DEBUG_MODE') is not None:
    debug_mode = os.getenv('DEBUG_MODE').upper() == "TRUE"  # type:ignore
    disable_debug_query = os.getenv('DISABLE_DEBUG_QUERY', "FALSE") == "TRUE"
    if debug_mode:
        logging.getLogger().setLevel(logging.INFO)
    if debug_mode and not disable_debug_query:
        logging.basicConfig()
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
