"""Export all middlewares from this module."""
from .exception import ExceptionCollector
from .cors import cors

middlewares = [
    ExceptionCollector,
]
