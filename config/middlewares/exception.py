# pylint: disable=broad-except
# pylint: disable=too-few-public-methods
"""Middleware to collect and handle exceptions from Exception to JSON Response."""
import traceback
from fastapi.responses import JSONResponse
from fastapi import Request, HTTPException, FastAPI
from starlette.exceptions import HTTPException as StarletteHTTPException
from sqlalchemy.exc import SQLAlchemyError


async def http_exception_handler(_req, exc) -> JSONResponse:
    """Exception handler to convert it as a JSON Response."""
    return JSONResponse(
        status_code=exc.status_code,
        content={'message': exc.detail}
    )


class ExceptionCollector:
    """Middleware to collect and handle exceptions from Exception to JSON Response."""

    def __init__(self, app: FastAPI):
        app.add_exception_handler(
            StarletteHTTPException, http_exception_handler)

    type = "http"

    async def __call__(self, request: Request, call_next):
        try:
            response = await call_next(request)
            return response
        except Exception as err:
            if isinstance(err, HTTPException):
                return JSONResponse(status_code=err.status_code, content={'message': err.detail})
            if isinstance(err, SQLAlchemyError):
                sql_err = err.__cause__
                return JSONResponse(
                    status_code=500,
                    content={'message': f"{sql_err.__class__.__name__} ({sql_err.pgcode}) "
                                        f"{sql_err.pgerror}"})
            traceback.print_exc()
            detail = traceback.format_exc()
            return JSONResponse(status_code=500,
                                content={'message': "Internal server error", 'detail': detail})
