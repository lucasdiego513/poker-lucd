# pylint: disable=broad-exception-caught
"""Database config package"""
import os
from typing import Any
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

load_dotenv()

db: dict[str, Any] = {
    "user": os.getenv('USER_DB'),
    "password": os.getenv('PASS_DB'),
    "host": os.getenv('HOST_DB'),
    "port": os.getenv('PORT_DB') or 5432,
    "schema": os.getenv('SCHEMA_DB'),
    "name": os.getenv('NAME_DB')
}

db['url'] = f'postgresql://{db["user"]}:{db["password"]}@{db["host"]}:{db["port"]}/{db["name"]}'

engine = create_engine(db['url'], pool_size=5, echo=False, pool_pre_ping=True)
Session = sessionmaker(bind=engine)


def get_session():
    """Função para geração da session."""
    session = Session()
    try:
        yield session
    except Exception:
        session.rollback()
    else:
        session.commit()
    finally:
        session.close()
