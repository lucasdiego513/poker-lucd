"""My info repository module."""
from sqlalchemy.orm import Session
from domain.models import MyInfo


def get_my_info(session: Session, my_id: str) -> MyInfo:
    """Get my information by my id"""
    return session.query(MyInfo).filter(MyInfo.my_id == my_id).first()


def update_my_info(session: Session, my_info: MyInfo) -> MyInfo:
    """Update my information"""

    session.merge(my_info)
    session.flush()

    return my_info
