"""Match repository module."""
from uuid import UUID
from sqlalchemy.orm import Session
from domain.models import Match


def get_all_matches(session: Session):
    """Get all matches"""
    return session.query(Match).all()


def get_match(session: Session, match_id: UUID):
    """Get a match by its id"""
    return session.query(Match).filter(Match.match_id == match_id).first()


def add_match(session: Session, match: Match):
    """Add a new match"""
    session.add(match)
    session.flush()

    return match


def update_match(session: Session, match: Match):
    """Update a match"""
    session.merge(match)
    session.flush()

    return match
