"""Service to deal with poker cards"""
# pylint: disable=line-too-long
# pylint: disable=too-many-return-statements
from enum import Enum
import random


class HandRank(Enum):
    """Represents the rank of a poker hand."""
    HIGH_CARD = "High Card"
    PAIR = "Pair"
    TWO_PAIR = "Two Pair"
    THREE_OF_A_KIND = "Three of a Kind"
    STRAIGHT = "Straight"
    FLUSH = "Flush"
    FULL_HOUSE = "Full House"
    FOUR_OF_A_KIND = "Four of a Kind"
    STRAIGHT_FLUSH = "Straight Flush"
    ROYAL_FLUSH = "Royal Flush"


class Deck:
    """Represents a deck of playing cards."""

    def __init__(self):
        """Initialize the deck with 52 cards."""
        # Possible suits and ranks
        suits = ['s', 'h', 'd', 'c']
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A']

        self.cards = [{'rank': rank, 'suit': suit} for rank in ranks for suit in suits]
        self.values = {rank: i + 2 for i, rank in enumerate(ranks)}
        random.shuffle(self.cards)

    def find_my_hand(self, cards):
        """Find my hand in the deck."""
        hand = [card for card in self.cards if f"{card['rank']}{card['suit']}" in cards]

        def sort_key(card):
            """Return the value of the card."""
            return self.card_value(card)

        # Sort my hand by value - descending order (from A to 2)
        hand.sort(key=sort_key, reverse=True)

        return hand

    def card_value(self, card):
        """Return the value of the card."""
        return self.values[card['rank']]

    def values_to_cards(self, values):
        """Return all cards corresponding to the values."""
        return [card for card in self.cards if self.card_value(card) in values]

    @staticmethod
    def card_to_str(card):
        """Return the string representation of the card."""
        return f"{card['rank']}{card['suit']}"


class PokerHand:
    """Represents a poker hand."""

    def __init__(self, cards):
        """Initialize the hand with 5 cards."""
        self.deck = Deck()
        self.cards = self.deck.find_my_hand(cards)
        self.hand_rank = None
        self.all_same_suit = self.check_all_suited()
        self.missing_to_flush, self.most_common_suit = self.compute_missing_to_flush()
        self.pairs = self.get_all_pairs()

    def __str__(self):
        """Return the string representation of the hand."""
        return ' '.join([f"{card['rank']}{card['suit']}" for card in self.cards])

    def __repr__(self):
        """Return the string representation of the hand."""
        return self.__str__()

    def rank_my_hand(self):
        """Rank the current hand according to poker hand rankings."""
        # My hand is already sorted by value in descending order (from A to 2)

        if self.is_royal_flush():
            self.hand_rank = HandRank.ROYAL_FLUSH
        elif self.is_straight_flush():
            self.hand_rank = HandRank.STRAIGHT_FLUSH
        elif self.is_four_of_a_kind():
            self.hand_rank = HandRank.FOUR_OF_A_KIND
        elif self.is_full_house():
            self.hand_rank = HandRank.FULL_HOUSE
        elif self.is_flush():
            self.hand_rank = HandRank.FLUSH
        elif self.is_straight():
            self.hand_rank = HandRank.STRAIGHT
        elif self.is_three_of_a_kind():
            self.hand_rank = HandRank.THREE_OF_A_KIND
        elif self.is_two_pair():
            self.hand_rank = HandRank.TWO_PAIR
        elif self.is_pair():
            self.hand_rank = HandRank.PAIR
        else:
            self.hand_rank = HandRank.HIGH_CARD

    def which_cards_to_swap(self, my_total_balance: int):
        """Compute which cards to swap based on the current hand and the poker rules.

        We are implementing a basic strategy:
            1 - If I have Royal Flush and Straight Flush, keep all cards and bet the maximum, i.e., 200.
            2 - If I have Four of a Kind, ask to swap the remaining card and bet the maximum, i.e., 200.
            3 - If I have Full House, keep all cards and bet 85% of the maximum, i.e., 170.
            4 - If I have Flush, keep all cards and bet 70% of the maximum, i.e., 140.
            5 - If I have Straight, apply the following rules:
                5.1 - If I have a Straight and my total balance is greater than 5000, ask to swap 3 random cards and bet 50% of the maximum, i.e., 100.
                5.2 - If I have a Straight and my total balance is less than 5000, keep all cards and bet 60% of the maximum, i.e., 120.
            6 - If I have Three of a Kind, ask to swap the remaining 2 cards and bet 40% of the maximum, i.e., 80.
            7 - If I have Two Pair, apply the following rules:
                7.1 - If I have Two Pair and my total balance is greater than 5000, ask to swap 3 cards, keeping one of the pairs, and bet 30% of the maximum, i.e., 60.
                7.2 - If I have Two Pair and my total balance is less than 5000, ask to swap the remaining card and bet 35% of the maximum, i.e., 70.
            8 - If I have a Pair, ask to swap the remaining 3 cards and bet 20% of the maximum, i.e., 40.
            9 - If I have High Card, ask to swap all cards and bet 10% of the maximum, i.e., 20.
        """

        repeating_cards = [card_value for card_value, count in self.pairs.items()]

        if self.hand_rank in (HandRank.ROYAL_FLUSH, HandRank.STRAIGHT_FLUSH):
            return []
        if self.hand_rank == HandRank.FOUR_OF_A_KIND:
            return [self.deck.card_to_str(card) for card in self.cards if self.deck.card_value(card) not in repeating_cards]
        if self.hand_rank == HandRank.FULL_HOUSE:
            return []
        if self.hand_rank == HandRank.FLUSH:
            return []
        if self.hand_rank == HandRank.STRAIGHT:
            if my_total_balance > 5000:
                return [self.deck.card_to_str(card) for card in random.sample(self.cards, 3)]
            return []
        if self.hand_rank == HandRank.THREE_OF_A_KIND:
            return [self.deck.card_to_str(card) for card in self.cards if self.deck.card_value(card) not in repeating_cards]
        if self.hand_rank == HandRank.TWO_PAIR:
            if my_total_balance > 5000:
                return [self.deck.card_to_str(card) for card in self.cards if self.deck.card_value(card) != repeating_cards[0]]
            return [self.deck.card_to_str(card) for card in self.cards if self.deck.card_value(card) not in repeating_cards]
        if self.hand_rank == HandRank.PAIR:
            if self.missing_to_flush == 1:
                return self.swap_try_flush()
            return [self.deck.card_to_str(card) for card in self.cards if self.deck.card_value(card) not in repeating_cards]

        # High card case
        if self.missing_to_flush == 1:
            return self.swap_try_flush()
        return [self.deck.card_to_str(card) for card in self.cards]

    def which_bet(self, balance) -> int:
        """
        Follow the strategy defined in which_cards_to_swap method to define the initial bet.
        """
        if self.hand_rank in (HandRank.ROYAL_FLUSH, HandRank.STRAIGHT_FLUSH):
            return 200
        if self.hand_rank == HandRank.FOUR_OF_A_KIND:
            return 200
        if self.hand_rank == HandRank.FULL_HOUSE:
            return 170
        if self.hand_rank == HandRank.FLUSH:
            return 140
        if self.hand_rank == HandRank.STRAIGHT:
            if balance > 5000:
                return 100
            return 120
        if self.hand_rank == HandRank.THREE_OF_A_KIND:
            return 80
        if self.hand_rank == HandRank.TWO_PAIR:
            if balance > 5000:
                return 60
            return 70
        if self.hand_rank == HandRank.PAIR:
            if self.missing_to_flush == 1:
                return 30
            return 40
        # High card case
        if self.missing_to_flush == 1:
            return 15
        return 20

    def is_royal_flush(self):
        """
        Check if the hand is a royal flush. A royal flush is a straight flush with the highest cards.

        This is A, K, Q, J, 10, all in the same suit.
        """
        return self.is_straight_flush() and self.cards[0]['rank'] == 'A'

    def is_straight_flush(self):
        """
        Check if the hand is a straight flush. A straight flush is a straight where all cards have the same suit.

        This is five cards in a sequence, all in the same suit, i.e., 5, 6, 7, 8, 9 of hearts.
        """
        return self.is_straight() and self.all_same_suit

    def is_four_of_a_kind(self):
        """
        Check if the hand is a four of a kind. A four of a kind is a hand where four cards have the same value.

        This is four cards of the same rank, i.e., 8, 8, 8, 8, A.
        """

        # from self.pairs, check if there is a pair with 4 cards
        return 4 in self.pairs.values() and len(self.pairs) == 1

    def is_full_house(self):
        """
        Check if the hand is a full house. A full house is a hand where three cards have the same value and the other two also have the same value.

        This is three of a kind and a pair, i.e., 8, 8, 8, A, A.
        """
        return 3 in self.pairs.values() and 2 in self.pairs.values()

    def is_flush(self):
        """
        Check if the hand is a flush. A flush is a hand where all cards have the same suit.

        This is five cards of the same suit, i.e., 5, 7, 8, 9, K of spades.
        """
        return self.all_same_suit

    def is_straight(self):
        """
        Check if the hand is a straight. A straight is a hand where all cards have consecutive values.

        This is five cards in a sequence, i.e., 5, 6, 7, 8, 9.

        Obs.: Aces can be the lowest card in a straight (5, 4, 3, 2, A), a.k.a wheel straight.
        """

        card_values = [self.deck.card_value(card) for card in self.cards]

        # Check if the hand is a wheel straight (5, 4, 3, 2, A)
        if card_values == [14, 5, 4, 3, 2]:
            return True
        for i in range(len(card_values) - 1):
            if card_values[i + 1] - card_values[i] != -1:
                return False
        return True

    def is_three_of_a_kind(self):
        """
        Check if the hand is a three of a kind. A three of a kind is a hand where three cards have the same value.

        This is three cards of the same rank, i.e., 8, 8, 8, A, K.
        """
        return 3 in self.pairs.values()

    def is_two_pair(self):
        """
        Check if the hand is a two pair. A two pair is a hand where two cards have the same value and another two cards have the same value.

        This is two cards of the same rank and another two cards of the same rank, i.e., 8, 8, K, K, A.
        """
        pairs_count = sum(1 for pair in self.pairs.values() if pair == 2)
        return pairs_count == 2

    def is_pair(self):
        """
        Check if the hand is a pair. A pair is a hand where two cards have the same value.

        This is two cards of the same rank, i.e., 8, 8, A, K, Q.
        """
        return 2 in self.pairs.values()

    # def is_high_card(self):
    #     """
    #     Check if the hand is a high card. A high card is a hand where the highest card is less than 10.
    #
    #     This is a hand where the highest card is less than 10, i.e., 2, 5, 8, J, A.
    #     """
    #     return self.deck.card_value(self.cards[0]) < 10

    def check_all_suited(self):
        """Check if all cards in the hand have the same suit."""
        return all(card['suit'] == self.cards[0]['suit'] for card in self.cards)

    def compute_missing_to_flush(self) -> (int, str):
        """ Return the number of cards missing to complete a flush """
        if self.all_same_suit:
            return 0, self.cards[0]['suit']
        # Count how many cards of each suit are already present in the hand
        suits_count = {'h': 0, 'd': 0, 'c': 0, 's': 0}
        for card in self.cards:
            suits_count[card['suit']] += 1

        # Find the suit with the most cards
        max_suit_count = max(suits_count.values())

        most_common_suit = max(suits_count, key=suits_count.get)

        return 5 - max_suit_count, most_common_suit

    def get_all_pairs(self):
        """
        This method returns all pairs in the hand.

        That is:
            - if I have 1, 1, 2, 2, 3, it will return {1:2, 2:2}.
            - if I have A, A, A, T, T, it will return {A:3, T:2}.
            - if I have J, J, 2, 5, 9, it will return {J:2}.
        """
        card_values = [self.deck.card_value(card) for card in self.cards]
        pairs = {}
        for card in card_values:
            if card_values.count(card) > 1:
                pairs[card] = card_values.count(card)
        return pairs

    def swap_try_flush(self):
        """Return the cards to swap to try to get a flush."""
        if self.missing_to_flush == 0:
            return []

        return [self.deck.card_to_str(card) for card in self.cards if card['suit'] != self.most_common_suit]
