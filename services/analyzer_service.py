"""Service with the logic to analyze and decide the best hand in the poker 5-card draw game."""
from domain.models import Match, MyInfo
from domain.schemas import FirstRoundInput, FirstRoundOutput
from domain.schemas.second_round_schemas import SecondRoundOutput
from .poker_service import PokerHand


def analyze_first_round(my_info: MyInfo, body: FirstRoundInput) -> FirstRoundOutput:
    """Analyze the first round of the poker match."""

    # Mount hands
    hand = PokerHand(body.cards)

    hand.rank_my_hand()

    swap = hand.which_cards_to_swap(my_info.balance)

    bet = hand.which_bet(my_info.balance)

    print(f"Information after first round analysis: "
          f"cards: {body.cards}; "
          f"hand_rank: {hand.hand_rank}; "
          f"cards_to_swap: {swap}; "
          f"initial_bet: {bet}; ")

    return FirstRoundOutput(
        bet=bet,
        cards_to_swap=swap
    )


def analyze_second_round(my_info: MyInfo, match: Match) -> (SecondRoundOutput, str):
    """Analyze the second round of the poker match.
    This analysis is based on the cards I have and the total value I'm betting."""

    hand = PokerHand(match.cards)

    hand.rank_my_hand()

    bet = hand.which_bet(my_info.balance)

    print(f"Information after second round analysis: "
          f"cards: {match.cards}; "
          f"hand_rank: {hand.hand_rank}; "
          f"bet: {bet}")

    return SecondRoundOutput(bet=bet), hand.hand_rank
