"""Poker match service."""
from sqlalchemy.orm import Session
from services import analyzer_service
from domain.schemas import FirstRoundInput, FirstRoundOutput, SecondRoundInput, \
    SecondRoundOutput, ResultInput, ReportSchema
from domain.models import Match
from domain.errors import ResourceNotFoundError
from consumers.dealer_consumer import join_match as consume_dealer_join_match
from repositories import match as match_repository
from repositories import my_info as my_info_repository
from config import my_info as lucd


def join_match() -> dict:
    """Consuming dealer's endpoint to join a poker match"""

    # Call the dealer's endpoint to join the match - My info is in the .env file
    join_info = {
        'id': lucd['id'],
        'name': lucd['name'],
        'publicApiUrl': lucd['public_api_url']
    }
    response = consume_dealer_join_match(join_info)

    status_code = response.status_code

    resp = {
        "joined_info": join_info,
    }

    if status_code == 204:
        resp['joined_status'] = \
            "Joined the match successfully - Status code 204 from dealer's consumer"
    else:
        resp['joined_status'] = \
            f"Failed to join the match - Status code {status_code} from dealer's consumer"

    return resp


def handle_first_round(session: Session, body: FirstRoundInput) -> FirstRoundOutput:
    """Handle the first round of the poker match"""
    # First get my info from the database
    my_info = my_info_repository.get_my_info(session, lucd['id'])

    # Store the match info in the database
    match = Match(**body.dict())

    # Call the analyzer to get the cards I should swap (or not) and the bet
    first_round_output = analyzer_service.analyze_first_round(my_info, body)

    match.cards_to_swap = first_round_output.cards_to_swap

    match_repository.add_match(session, match)

    return first_round_output


def handle_second_round(session: Session, body: SecondRoundInput) -> SecondRoundOutput:
    """Handle the second round of the poker match"""
    # First get my info from the database
    my_info = my_info_repository.get_my_info(session, lucd['id'])

    # Get the match from the database
    match = match_repository.get_match(session, body.match_id)

    if not match:
        raise ResourceNotFoundError(f"Match not found for id {body.match_id}")

    # Perform swap if I asked for it
    if len(match.cards_to_swap) > 0:
        # Check if dealer gave me the right number of cards to swap
        if len(match.cards_to_swap) != len(body.new_cards):
            raise ValueError("Dealer gave me the wrong number of cards to swap")

        # Substitute the cards I want to swap
        match.cards = [card for card in match.cards if card not in match.cards_to_swap]
        match.cards.extend(body.new_cards)

    # Analyze the second round base on my cards and the total value I'm betting
    second_round_output, hand_rank = analyzer_service.analyze_second_round(my_info, match)

    match.rank = hand_rank.value

    match_repository.update_match(session, match)

    return second_round_output


def handle_result(session: Session, body: ResultInput) -> None:
    """Handle the result of the poker match"""

    # Get the match from the database
    match = match_repository.get_match(session, body.match_id)

    if not match:
        raise ResourceNotFoundError(f"Match not found for id {body.match_id}")

    # Update the match with the result
    match.result = body.result

    # Update the match balance
    match.balance = body.your_match_balance

    match_repository.update_match(session, match)

    update_my_balance(session, body.your_match_balance)


def update_my_balance(session: Session, match_balance: int) -> None:
    """Update my balance in the database"""

    my_info = my_info_repository.get_my_info(session, lucd['id'])

    # New balance is the current balance plus the match balance
    my_info.balance = my_info.balance + match_balance

    my_info_repository.update_my_info(session, my_info)


def get_report(session) -> list[ReportSchema]:
    """Get a report of all poker matches with each opponent,
    result and my balance after each match"""

    # Get all matches from the database
    matches = match_repository.get_all_matches(session)

    # Group all matches by opponent
    matches_by_opponent = {}
    for match in matches:
        if match.opponent_id not in matches_by_opponent:
            matches_by_opponent[match.opponent_id] = []

        try:
            matches_by_opponent[match.opponent_id].append(
                {
                    "matchId": match.match_id,
                    "myRank": match.rank,
                    "result": match.result,
                    "myMatchBalance": match.balance
                }
            )
        # pylint: disable=broad-except
        except Exception as _:
            pass

    # Create the report
    report = []
    for opponent_id, matches in matches_by_opponent.items():
        total_balance_with_opponent = sum(match['myMatchBalance'] for match in matches)
        report.append(ReportSchema(opponent_id=opponent_id, matches=matches,
                                   total_balance_with_opponent=total_balance_with_opponent))

    return report
