"""Utils to validate variables."""


def valuable(value):
    """Check if value None"""
    return value is not None
